$(document).ready(function(){$('#external-events div.external-event').each(function(){var eventObject={title:$.trim($(this).text())// use the element's text as the event title
			};
			
			// store the Event Object in the DOM element so we can get to it later
			$(this).data('eventObject', eventObject);
			
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
			
		});
	
	
		/* initialize the calendar
		-----------------------------------------------------------------*/
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true, // this allows things to be dropped onto the calendar !!!
			drop: function(date, allDay) { // this function is called when something is dropped
			
				// retrieve the dropped element's stored Event Objectvar originalEventObject=$(this).data('eventObject');var copiedEventObject=$.extend({},originalEventObject);copiedEventObject.start=date;copiedEventObject.allDay=allDay;$('#calendar').fullCalendar('renderEvent',copiedEventObject,true);if($('#drop-remove').is(':checked')){$(this).remove();}}});$('.fc-header').hide();var currentDate=$('#calendar').fullCalendar('getDate');$('#calender-current-day').html($.fullCalendar.formatDate(currentDate,"dddd"));$('#calender-current-date').html($.fullCalendar.formatDate(currentDate,"MMM yyyy"));$('#calender-prev').click(function(){$('#calendar').fullCalendar('prev');currentDate=$('#calendar').fullCalendar('getDate');$('#calender-current-day').html($.fullCalendar.formatDate(currentDate,"dddd"));$('#calender-current-date').html($.fullCalendar.formatDate(currentDate,"MMM yyyy"));});$('#calender-next').click(function(){$('#calendar').fullCalendar('next');currentDate=$('#calendar').fullCalendar('getDate');$('#calender-current-day').html($.fullCalendar.formatDate(currentDate,"dddd"));$('#calender-current-date').html($.fullCalendar.formatDate(currentDate,"MMM yyyy"));});$('#change-view-month').click(function(){$('#calendar').fullCalendar('changeView','month');});$('#change-view-week').click(function(){$('#calendar').fullCalendar('changeView','agendaWeek');});$('#change-view-day').click(function(){$('#calendar').fullCalendar('changeView','agendaDay');});});