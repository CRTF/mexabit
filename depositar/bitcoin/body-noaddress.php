<div class="page-container row-fluid">
   
   <!-- Sidebar -->
   <div class="page-sidebar" id="main-menu">
      <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        
         <p class="menu-title">MENÚ</p>
         <ul>
            <!-- dashboard -->
            <li class="start "> <a href="../../dashboard"> <i class="icon-custom-home"></i> <span class="title">Dashboard</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- end dashboard -->
            <!-- Comprar/Vender -->
            <li class=" ">
               <a href="javascript:;"> <i class="fa fa fa-bitcoin"></i> <span class="title">Comprar / Vender</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../../comprar/bitcoin">Comprar Bitcoins </a> </li>
                  <li> <a href="">Vender Bitcoins</a> </li>
               </ul>
            </li>
            <!-- End Comprar/Vender -->
            <!-- Depositar -->
            <li class="active open">
               <a href="javascript:;"> <i class="fa fa fa-download"></i> <span class="title">Depositar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li  class="active"> <a href="">Depositar Bitcoins </a> </li>
                  <li> <a href="../pesos">Depositar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Depositar -->
            <!-- Retirar -->
            <li class="">
               <a href="javascript:;"> <i class="fa fa fa-upload"></i> <span class="title">Retirar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../retirar/bitcoin">Retirar Bitcoins </a> </li>
                  <li> <a href="../retirar/pesos">Retirar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Retirar -->
            <!-- Enviar -->
            <li class="start "> <a href="index.html"> <i class="fa fa-rocket"></i> <span class="title">Enviar</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- End Enviar-->
            <li class="hidden-lg hidden-md hidden-xs" id="more-widgets">
               <a href="javascript:;"> <i class="fa fa-plus"></i></a>
               <ul class="sub-menu">
                  <li class="side-bar-widgets">
                     <p class="menu-title">CUENTA <span class="pull-right"><a href="index.html#" class="create-folder"><i class="icon-plus"></i></a></span></p>
                     <ul class="folders">
                        <li>
                           <a href="index.html#">
                              <div class="status-icon green"></div>
                              Datos Personales 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon red"></div>
                              To do list 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon blue"></div>
                              Projects 
                           </a>
                        </li>
                        <li class="folder-input" style="display:none">
                           <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="" id="folder-name">
                        </li>
                     </ul>
                     <p class="menu-title">PROJECTS </p>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">Freelancer<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Redesign home page</p>
                        </div>
                     </div>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">envato<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Statistical report</p>
                        </div>
                     </div>
                  </li>
               </ul>
            </li>
         </ul>
         <div class="side-bar-widgets">
            <p class="menu-title">CUENTA</p>
            <ul class="folders">
               <li>
                  <a href="index.html#">
                     <div class="status-icon green"></div>
                     Datos Personales 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon red"></div>
                     Cuentas Bancarias 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon yellow"></div>
                     Contactos
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon blue"></div>
                     Historial
                  </a>
               </li>
               <li class="folder-input" style="display:none">
                  <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="">
               </li>
            </ul>
            <p class="menu-title">PROJECTS </p>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">Freelancer<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                  <p>Redesign home page</p>
               </div>
            </div>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">envato<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                  <p>Statistical report</p>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="footer-widget">
      <div class="progress transparent progress-small no-radius no-margin">
         <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar"></div>
      </div>
      <div class="pull-right">
         <div class="details-status"> <span data-animation-duration="560" data-value="86" class="animate-number"></span>% </div>
         <a href="lockscreen.html"><i class="fa fa-power-off"></i></a>
      </div>
   </div>

   <!-- End Sidebar -->

   <!-- Page Content -->
   <div class="page-content">
      
      <div id="portlet-config" class="modal hide">
         <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Widget Settings</h3>
         </div>
         <div class="modal-body"> Widget settings form goes here </div>
      </div>

      <div class="clearfix"></div>


      <div class="content sm-gutter"><!-- Content sm-gutter -->
        
        <!-- Breadcrumbs -->
      	<ul class="breadcrumb">
			<li>
			<p>Depositar</p>
			</li>
			<li><a href="" class="active">Bitcoins</a> </li>
		</ul>
		<div class="page-title"> <i class="icon-custom-right"></i>
			<h3>Depositar <span class="semi-bold">Bitcoins</span></h3>
		</div>
		<!-- End Breadcrumbs -->

        <div class="row">
            <div class="col-md-12">
			   <div class="grid simple">
			      <div class="grid-title no-border">
			         <h4><span class="semi-bold">Depositar Bitcoins</span></h4>
			         <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="javascript:;" class="remove"></a> </div>
			      				      	
			      </div>
			      <div class="grid-body no-border">
			         
			         <h4>Para depositar Bitcoins en tu cuenta de Mexibit, envíalos a la siguiente dirección:</h4>
                     <div class="input-group input-group-lg" id="deposit-address-container">
                        <span class="input-group-addon"><i class="fa fa-bitcoin"></i></span>
                        <input type="text" class="form-control default-cursor" value="" readonly >
                     </div>

                    
			      </div>
               <div class="row" style="margin-top:15px">
                  <div class="col-md-6 ">
                     <form action="" method="post">
                              <button style="width: 100%;" class="btn btn-lg btn-success" type="submit" name="newaddress">
                                 <i class="fa fa-download fa-2x pull-left"></i>
                                 <div style="margin-top: 15px">
                                    GENERAR DIRECCIÓN DE DEPÓSITO
                                 </div>
                              </button>
                     </form>
                  </div>
               </div>
			   </div>
			</div>
            
            
           
        </div>    

      </div><!-- Content sm-gutter -->
   </div><!-- End Page content -->   
</div>