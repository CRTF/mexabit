<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>Comprar Bitcoins | Mexabit</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
      <meta content="" name="description"/>
      <meta content="" name="author"/>
      <link href="../../assets/plugins/jquery-metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" type="text/css" href="../../assets/plugins/shape-hover/css/demo.css"/>
      <link rel="stylesheet" type="text/css" href="../../assets/plugins/shape-hover/css/component.css"/>
      <link rel="stylesheet" type="text/css" href="../../assets/plugins/owl-carousel/owl.carousel.css"/>
      <link rel="stylesheet" type="text/css" href="../../assets/plugins/owl-carousel/owl.theme.css"/>
      <link href="../../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="../../assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen"/>
      <link rel="stylesheet" href="../../assets/plugins/jquery-ricksaw-chart/css/rickshaw.css" type="text/css" media="screen">
      <link rel="stylesheet" href="../../assets/plugins/Mapplic/mapplic/mapplic.css" type="text/css" media="screen">
      <link href="../../assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/css/style.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/css/responsive.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
      <link href="../../assets/css/magic_space.css" rel="stylesheet" type="text/css"/>
      <style type="text/css">
      		#form1Amount{
      			width: 40%;
      		}
            input[readonly].default-cursor {
               cursor: default;
            }
            #deposit-address-container{
               margin-top: 20px;
             }
      </style>
   </head>
   <body class="">
      <div class="header navbar navbar-inverse ">
         <div class="navbar-inner">
            <div class="header-seperation">
               <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">
                  <li class="dropdown">
                     <a id="main-menu-toggle" href="#main-menu" class="">
                        <div class="iconset top-menu-toggle-white"></div>
                     </a>
                  </li>
               </ul>
               <a href="index.html"><img src="../../assets/img/logo.png" class="logo" alt="" data-src="../../assets/img/logo.png" data-src-retina="../../assets/img/logo2x.png" width="106" height="21"/></a>
               <ul class="nav pull-right notifcation-center">
                  <li class="dropdown" id="header_task_bar">
                     <a href="index.html" class="dropdown-toggle active" data-toggle="">
                        <div class="iconset top-home"></div>
                     </a>
                  </li>
                  <li class="dropdown" id="header_inbox_bar">
                     <a href="email.html" class="dropdown-toggle">
                        <div class="iconset top-messages"></div>
                        <span class="badge" id="msgs-badge">2</span> 
                     </a>
                  </li>
                  <li class="dropdown" id="portrait-chat-toggler" style="display:none">
                     <a href="index.html#sidr" class="chat-menu-toggle">
                        <div class="iconset top-chat-white "></div>
                     </a>
                  </li>
               </ul>
            </div>
            <div class="header-quick-nav">
               <div class="pull-left">
                  <ul class="nav quick-section">
                     <li class="quicklinks">
                        <a href="javascript:;" class="" id="layout-condensed-toggle">
                           <div class="iconset top-menu-toggle-dark"></div>
                        </a>
                     </li>
                  </ul>
                  <ul class="nav quick-section">
                     <li class="quicklinks">
                        <a href="index.html#" class="">
                           <div class="iconset top-reload"></div>
                        </a>
                     </li>
                     <li class="quicklinks"> <span class="h-seperate"></span></li>
                     <li class="quicklinks">
                        <a href="index.html#" class="">
                           <div class="iconset top-tiles"></div>
                        </a>
                     </li>
                     <li class="m-r-10 input-prepend inside search-form no-boarder"> <span class="add-on"> <span class="iconset top-search"></span></span>
                        <input name="" type="text" class="no-boarder " placeholder="Search Dashboard" style="width:250px;">
                     </li>
                  </ul>
               </div>
               <div class="pull-right">
                  <div class="chat-toggler">
                     <a href="index.html#" class="dropdown-toggle" id="my-task-list" data-placement="bottom" data-content='' data-toggle="dropdown" data-original-title="Notifications">
                        <div class="user-details">
                           <div class="username"> <span class="badge badge-important">3</span> John <span class="bold">Smith</span> </div>
                        </div>
                        <div class="iconset top-down-arrow"></div>
                     </a>
                     <div id="notification-list" style="display:none">
                        <div style="width:300px">
                           <div class="notification-messages info">
                              <div class="user-profile"> <img src="../../assets/img/profiles/d.jpg" alt="" data-src="../../assets/img/profiles/d.jpg" data-src-retina="../../assets/img/profiles/d2x.jpg" width="35" height="35"> </div>
                              <div class="message-wrapper">
                                 <div class="heading"> David Nester - Commented on your wall </div>
                                 <div class="description"> Meeting postponed to tomorrow </div>
                                 <div class="date pull-left"> A min ago </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                           <div class="notification-messages danger">
                              <div class="iconholder"> <i class="icon-warning-sign"></i> </div>
                              <div class="message-wrapper">
                                 <div class="heading"> Server load limited </div>
                                 <div class="description"> Database server has reached its daily capicity </div>
                                 <div class="date pull-left"> 2 mins ago </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                           <div class="notification-messages success">
                              <div class="user-profile"> <img src="../../assets/img/profiles/h.jpg" alt="" data-src="../../assets/img/profiles/h.jpg" data-src-retina="../../assets/img/profiles/h2x.jpg" width="35" height="35"> </div>
                              <div class="message-wrapper">
                                 <div class="heading"> You haveve got 150 messages </div>
                                 <div class="description"> 150 newly unread messages in your inbox </div>
                                 <div class="date pull-left"> An hour ago </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                     <div class="profile-pic"> <img src="../../assets/img/profiles/avatar_small.jpg" alt="" data-src="../../assets/img/profiles/avatar_small.jpg" data-src-retina="../../assets/img/profiles/avatar_small2x.jpg" width="35" height="35"/> </div>
                  </div>
                  <ul class="nav quick-section ">
                     <li class="quicklinks">
                        <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="index.html#" id="user-options">
                           <div class="iconset top-settings-dark "></div>
                        </a>
                        <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                           <li><a href="user-profile.html"> My Account</a> </li>
                           <li><a href="calender.html">My Calendar</a> </li>
                           <li><a href="email.html"> My Inbox&nbsp;&nbsp;<span class="badge badge-important animated bounceIn">2</span></a> </li>
                           <li class="divider"></li>
                           <li><a href="login.html"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
                        </ul>
                     </li>
                     <li class="quicklinks"> <span class="h-seperate"></span></li>
                     <li class="quicklinks">
                        <a id="chat-menu-toggle" href="index.html#sidr" class="chat-menu-toggle">
                           <div class="iconset top-chat-dark "><span class="badge badge-important hide" id="chat-message-count">1</span></div>
                        </a>
                        <div class="simple-chat-popup chat-menu-toggle hide">
                           <div class="simple-chat-popup-arrow"></div>
                           <div class="simple-chat-popup-inner">
                              <div style="width:100px">
                                 <div class="semi-bold">David Nester</div>
                                 <div class="message">Hey you there </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>