<div class="page-container row-fluid">
   
   <!-- Sidebar -->
   <div class="page-sidebar" id="main-menu">
      <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        
         <p class="menu-title">MENÚ</p>
         <ul>
            <!-- dashboard -->
            <li class="start "> <a href="../../dashboard"> <i class="icon-custom-home"></i> <span class="title">Dashboard</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- end dashboard -->
            <!-- Comprar/Vender -->
            <li class=" ">
               <a href="javascript:;"> <i class="fa fa fa-bitcoin"></i> <span class="title">Comprar / Vender</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../../comprar/bitcoin">Comprar Bitcoins </a> </li>
                  <li> <a href="">Vender Bitcoins</a> </li>
               </ul>
            </li>
            <!-- End Comprar/Vender -->
            <!-- Depositar -->
            <li class="active open">
               <a href="javascript:;"> <i class="fa fa fa-download"></i> <span class="title">Depositar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li  > <a href="../bitcoin">Depositar Bitcoins </a> </li>
                  <li class="active"> <a href="">Depositar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Depositar -->
            <!-- Retirar -->
            <li class="">
               <a href="javascript:;"> <i class="fa fa fa-upload"></i> <span class="title">Retirar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../retirar/bitcoin">Retirar Bitcoins </a> </li>
                  <li> <a href="../retirar/pesos">Retirar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Retirar -->
            <!-- Enviar -->
            <li class="start "> <a href="index.html"> <i class="fa fa-rocket"></i> <span class="title">Enviar</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- End Enviar-->
            <li class="hidden-lg hidden-md hidden-xs" id="more-widgets">
               <a href="javascript:;"> <i class="fa fa-plus"></i></a>
               <ul class="sub-menu">
                  <li class="side-bar-widgets">
                     <p class="menu-title">CUENTA <span class="pull-right"><a href="index.html#" class="create-folder"><i class="icon-plus"></i></a></span></p>
                     <ul class="folders">
                        <li>
                           <a href="index.html#">
                              <div class="status-icon green"></div>
                              Datos Personales 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon red"></div>
                              To do list 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon blue"></div>
                              Projects 
                           </a>
                        </li>
                        <li class="folder-input" style="display:none">
                           <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="" id="folder-name">
                        </li>
                     </ul>
                     <p class="menu-title">PROJECTS </p>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">Freelancer<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Redesign home page</p>
                        </div>
                     </div>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">envato<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Statistical report</p>
                        </div>
                     </div>
                  </li>
               </ul>
            </li>
         </ul>
         <div class="side-bar-widgets">
            <p class="menu-title">CUENTA</p>
            <ul class="folders">
               <li>
                  <a href="index.html#">
                     <div class="status-icon green"></div>
                     Datos Personales 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon red"></div>
                     Cuentas Bancarias 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon yellow"></div>
                     Contactos
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon blue"></div>
                     Historial
                  </a>
               </li>
               <li class="folder-input" style="display:none">
                  <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="">
               </li>
            </ul>
            <p class="menu-title">PROJECTS </p>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">Freelancer<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                  <p>Redesign home page</p>
               </div>
            </div>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">envato<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                  <p>Statistical report</p>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="footer-widget">
      <div class="progress transparent progress-small no-radius no-margin">
         <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar"></div>
      </div>
      <div class="pull-right">
         <div class="details-status"> <span data-animation-duration="560" data-value="86" class="animate-number"></span>% </div>
         <a href="lockscreen.html"><i class="fa fa-power-off"></i></a>
      </div>
   </div>

   <!-- End Sidebar -->

   <!-- Page Content -->
   <div class="page-content">
      
      <div id="portlet-config" class="modal hide">
         <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Widget Settings</h3>
         </div>
         <div class="modal-body"> Widget settings form goes here </div>
      </div>

      <div class="clearfix"></div>


      <div class="content sm-gutter"><!-- Content sm-gutter -->
        
        <!-- Breadcrumbs -->
      	<ul class="breadcrumb">
			<li>
			<p>Depositar</p>
			</li>
			<li><a href="" class="active">Pesos</a> </li>
		</ul>
		<div class="page-title"> <i class="icon-custom-right"></i>
			<h3>Depositar <span class="semi-bold">Pesos</span></h3>
		</div>
		<!-- End Breadcrumbs -->

        <div class="row">
            <div class="col-md-12">
			   <div class="grid simple">
			      <div class="grid-title no-border">
			         <h4><span class="semi-bold">Depositar Pesos</span></h4>
			         <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="javascript:;" class="remove"></a> </div>
			      				      	
			      </div>
			      <div class="grid-body no-border">
			         
			         <div class="grid-title">

<!-- Wizard -->

<h4>Form <span class="semi-bold">Wizard</span></h4>
<div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="form_validations.html#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
</div>
<div class="grid-body ">
<div class="row">
<form novalidate="novalidate" id="commentForm">
<div id="rootwizard" class="col-md-12">
<div class="form-wizard-steps">
<ul class="wizard-steps form-wizard">
<li class="active" data-target="#step1"> <a href="form_validations.html#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Método</span> </a> </li>
<li data-target="#step2" class=""> <a href="form_validations.html#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Generar ficha</span> </a> </li>
<li data-target="#step3" class=""> <a href="form_validations.html#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">User settings</span> </a> </li>
<li data-target="#step4" class=""> <a href="form_validations.html#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">Feedback <br>
</span> </a> </li>
</ul>
<div class="clearfix"></div>
</div>
<div class="tab-content transparent">
<div class="tab-pane active" id="tab1"> <br>
<h4 class="semi-bold">Paso 1 - <span class="light">Selecciona el método de pago</span></h4>
<br>
<div class="row form-row">
<div class="col-md-12">
<input placeholder="Full Name" class="form-control no-boarder " name="txtFullName" id="txtFullName" type="text">
</div>
</div>
<div class="row form-row">
<div class="col-md-6">
<input placeholder="First Name" class="form-control no-boarder " name="txtFirstName" id="txtFirstName" type="text">
</div>
<div class="col-md-6">
<input placeholder="Last Name" class="form-control no-boarder " name="txtLastName" id="txtLastName" type="text">
</div>
</div>
</div>
<div class="tab-pane" id="tab2"> <br>
<h4 class="semi-bold">Step 2 - <span class="light">Account Information</span></h4>
<br>
<div class="row form-row">
<div class="col-md-8">
<input placeholder="Country" class="form-control no-boarder " name="txtCountry" id="txtCountry" type="text">
</div>
<div class="col-md-4">
<input placeholder="Postal Code" class="form-control no-boarder " name="txtPostalCode" id="txtPostalCode" type="text">
</div>
</div>
<div class="row form-row">
<div class="col-md-4">
<input placeholder="+94" class="form-control no-boarder " name="txtPhoneCode" id="txtPhoneCode" type="text">
</div>
<div class="col-md-8">
<input placeholder="Phone Number" class="form-control no-boarder " name="txtPhoneNumber" id="txtPhoneNumber" type="text">
</div>
</div>
</div>
<div class="tab-pane" id="tab3"> <br>
<h4 class="semi-bold">Step 3 - <span class="light">User Settings</span></h4>
<br>
</div>
<div class="tab-pane" id="tab4"> <br>
<h4 class="semi-bold">Step 4 - <span class="light">Feedback</span></h4>
<br>
</div>
<ul class=" wizard wizard-actions">
<li class="previous first disabled" style="display:none;"><a href="javascript:;" class="btn">&nbsp;&nbsp;First&nbsp;&nbsp;</a></li>
<li class="previous disabled"><a href="javascript:;" class="btn">&nbsp;&nbsp;Previous&nbsp;&nbsp;</a></li>
<li class="next last" style="display:none;"><a href="javascript:;" class="btn btn-primary">&nbsp;&nbsp;Last&nbsp;&nbsp;</a></li>
<li class="next"><a href="javascript:;" class="btn btn-primary">&nbsp;&nbsp;Next&nbsp;&nbsp;</a></li>
</ul>
</div>
</div>
</form>
</div>
</div>

<!-- end wizard -->
			      </div>
			   </div>
			</div>
            
            
           
        </div>    

      </div><!-- Content sm-gutter -->
   </div><!-- End Page content -->   
</div>