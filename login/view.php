<!DOCTYPE html>
<head>
   <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
   <meta charset="utf-8"/>
   <title>Webarch - Responsive Frontend</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
   <meta content="" name="description"/>
   <meta content="" name="author"/>
   
   <link rel="stylesheet" type="text/css" href="../assets/frontend/plugins/owl-carousel/owl.carousel.css"/>
   <link rel="stylesheet" type="text/css" href="../assets/frontend/plugins/owl-carousel/owl.theme.css"/>
   <link href="../assets/frontend/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
   <link href="../assets/frontend/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets/frontend/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets/frontend/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets/frontend/css/style.css" rel="stylesheet" type="text/css"/>
   <link href="../assets/frontend/css/responsive.css" rel="stylesheet" type="text/css"/>
   <link href="../assets/frontend/css/animate.min.css" rel="stylesheet" type="text/css"/>
   <script type="text/javascript" src="../assets/frontend/plugins/jquery-1.8.3.min.js"></script>

   <style type="text/css">
   	body{
   		background: url('..//assets/frontend/img/bg/mexabit-01-min.png');
   	}

   </style>
</head>
<body>
   <div class="main-wrapper">
      <div role="navigation" class="navbar navbar-default">
         <div class="container">
            <div class="compressed">
               <div class="navbar-header">
                  <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a href="contact.html#" class="navbar-brand compressed"><img src="../assets/frontend/img/logo_condensed.png" alt="" data-src="../assets/frontend/img/logo_condensed.png" data-src-retina="../assets/frontend/img/logo2x.png" width="119" height="22"/></a>
               </div>
               <div class="navbar-collapse collapse">
                  <ul class="nav navbar-nav navbar-right">
                     <li><a href="index.html">Home</a></li>
                     <li><a href="tour.html">Tour</a></li>
                     <li><a href="pricing.html">Pricing</a></li>
                     <li><a href="portfolio.html">Portfolio</a></li>
                     <li><a href="contact.html">Contact</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="section first p-b-60" >
         <div class="container">
               
               
               <div class="row">
                  <div class="col-md-4 col-md-offset-4 p-t-5 p-b-60" style="background-color:#FFF;">
                  	<form action="" method="post">
                       

                  	<div class="row form-row m-b-10">
                        <div class="col-md-10 col-md-offset-1">                        	
                           <h2 class="p-t-40"><span class="semi-bold" style="color:#F35958">Iniciar sesión</span></h2>       
                        </div>
                     </div>

                     <div class="row form-row m-b-10">
                        <div class="col-md-10 col-md-offset-1">                           
                           <div><?php echo $alert; ?></div>      
                        </div>
                     </div>                                      
                     
                     
                     <div class="row form-row m-b-20">
                        <div class="col-md-10 col-md-offset-1">
                           <label>Email:</label>
                           <input name="email_login" type="email" class="form-control" value="<?php echo $_POST['email_login']; ?>" placeholder="E-mail" required autofocus>
                        </div>
                     </div>

                     <div class="row form-row m-b-20">
                        <div class="col-md-10 col-md-offset-1">
                        	<label>Contraseña:</label>
                           <input name="password_login" type="password" class="form-control" placeholder="Contraseña" required>
                        </div>
                     </div>

                     <div class="row form-row m-b-20">
                        <div class="col-md-4 col-md-offset-1">
                           <button type="submit" name="submit_login" class="btn btn-danger btn-cons">Iniciar sesión</button>
                        </div>
                     </div>
                    

                      <div class="row form-row">
                        <div class="col-md-10 col-md-offset-1">
                        	<label>¿Olvidaste tu contraseña?</label>                           
                        </div>
                     </div>

                      </form>
                     
                        
                           
                        
                  </div>
               
               </div>
            
         </div>
      </div>
   

   <div class="section black">
<div class="container">
<div class="p-t-50 p-b-50">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<h2 class="text-center text-white m-b-30">¿No tienes cuenta?</h2>

		<div class="row form-row" align="center">

			<div class="col-md-12">
				<button type="button " class="btn btn-primary btn-cons"><h3 class="text-center text-white m-b-25"><span class="semi-bold">¡Registrate!</span></h3></button>
			</div>

		</div>

		<div class="clearfix"></div>


	</div>

</div>
</div>
</div>
</div>


    
      <div class="section white footer">
         <div class="container">
            <div class="p-t-30 p-b-50">
               <div class="row">
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 xs-m-b-20">
                     <img src="../assets/frontend/img/logo_condensed.png" alt="" data-src="../assets/frontend/img/logo_condensed.png" data-src-retina="../assets/frontend/img/logo2x.png" width="119" height="22"/>
                  </div>
                  <div class="col-md-4 col-lg-3 col-sm-4  col-xs-12 xs-m-b-20">
                     <address class="xs-no-padding  col-md-6 col-lg-6 col-sm-6  col-xs-12">
                        Crossraid<br>
                        85/B Cross Street,<br>
                        New York, USA<br>
                        NA1 42SL
                     </address>
                     <div class="xs-no-padding col-md-6 col-lg-6 col-sm-6">
                        <div>(0039) 389 957 5552</div>
                        <a class="__cf_email__" href="../cdn-cgi/l/email-protection.html" data-cfemail="d0a3a5a0a0bfa2a490a2b5a6bfa8feb9bf">[email&#160;protected]</a><script cf-hash='f9e31' type="text/javascript">
                           /* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("cf-hash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}}}catch(u){}}();/* ]]> */
                        </script>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-2  col-xs-12 xs-m-b-20">
                     Copyright © 2013
                     Privacy Policy
                     Design by Revox
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-2  col-xs-12 xs-m-b-20">
                     <div class="bold">RECRUITMENT</div>
                     We are frequently on the lookout for new talent!
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-2  col-xs-12 ">
                     <div class="bold">FOLLOW US</div>
                     We are frequently on the lookout for new talent!
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript" src="../assets/frontend/plugins/jquery-1.8.3.min.js"></script>
   <script src="../assets/frontend/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="../assets/frontend/plugins/pace/pace.min.js" type="text/javascript"></script>
   <script src="../assets/frontend/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
   <script src="../assets/frontend/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
   <script type="text/javascript" src="../assets/frontend/plugins/jquery-nicescroll/jquery.nicescroll.min.js"></script>
   <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
   <script src="../assets/frontend/plugins/jquery-gmap/gmaps.js" type="text/javascript"></script>
   <script src="../assets/frontend/js/google_maps.js" type="text/javascript"></script>
   <script type="text/javascript" src="../assets/frontend/js/core.js"></script>
</body>
</html>