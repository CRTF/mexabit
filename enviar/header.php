<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>Enviar | Mexabit</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
      <meta content="" name="description"/>
      <meta content="" name="author"/>
      <link href="../assets/plugins/jquery-metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" type="text/css" href="../assets/plugins/shape-hover/css/demo.css"/>
      <link rel="stylesheet" type="text/css" href="../assets/plugins/shape-hover/css/component.css"/>
      <link rel="stylesheet" type="text/css" href="../assets/plugins/owl-carousel/owl.carousel.css"/>
      <link rel="stylesheet" type="text/css" href="../assets/plugins/owl-carousel/owl.theme.css"/>
      <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="../assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen"/>
      <link rel="stylesheet" href="../assets/plugins/jquery-ricksaw-chart/css/rickshaw.css" type="text/css" media="screen">
      <link rel="stylesheet" href="../assets/plugins/Mapplic/mapplic/mapplic.css" type="text/css" media="screen">
      <link href="../assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/css/style.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/css/magic_space.css" rel="stylesheet" type="text/css"/>
      <style type="text/css">
      	

         .dropdown a:link{
            color:black;
            text-decoration:none;
         }
         .quick-section a{
            color:black;
            text-decoration:none;
         }

         .input{
            width: 40%;
            min-width: 80px;
         }
         #alert{
            font-size: 16px;
         }

      </style>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      <script src="../assets/js/money.js"></script>

      <script type="text/javascript">
      $(document).on("ready", function(){

         function loadbtcbalance () {
            $.ajax({
                url:"../functions/get_user_btc_balance.php",
                success:function(result){                     
                    $("#btc-balance").html(result);
                    setTimeout(loadbtcbalance, 1000);
                }
            });
        }
        loadbtcbalance();

        function loadmxnbalance () {
            $.ajax({
                url:"../functions/get_user_mxn_balance.php",
                success:function(result){
                    result = accounting.formatMoney(result);                    
                    $("#mxn-balance").html(result);
                    setTimeout(loadmxnbalance, 1000);
                }
            });
        }
        loadmxnbalance();
       

        $('#sendForm').submit(function(){
          
          $.ajax({
              type: 'POST',
              url: 'send_process.php',
              data: $(this).serialize()
          })
          .done(function(data){            
              $('#alert').html(data);
              $( '#sendForm' ).each(function(){
                this.reset();
              });              
          })
          .fail(function() {             
              alert( "Ocurrió un error al intentar realizar la operación." );            
          }); 
          // to prevent refreshing the whole page page
          return false;
   
        });

       

        $("#amount").on("keyup", function(){
          var amount = $("#amount").val();
          $("#order-amount").text(amount);

          var method = $("#method").val();
          if(method === "mxn"){
            $("#order-method").text("Pesos");
          } else if ( method === "btc"){
            $("#order-method").text("Bitcoin");
          }

          var email = $("#email").val();
          $("#order-email").text(email);

        });

        $("#email").on("keyup", function(){
          var amount = $("#amount").val();
          $("#order-amount").text(amount);

          var method = $("#method").val();
          if(method === "mxn"){
            $("#order-method").text("Pesos");
          } else if ( method === "btc"){
            $("#order-method").text("Bitcoin");
          }

          var email = $("#email").val();
          $("#order-email").text(email);

        });
      
        $("#method").on("change", function(){
          var method = $("#method").val();
          if(method === "mxn"){
            $("#order-method").text("Pesos");
          } else if ( method === "btc"){
            $("#order-method").text("Bitcoin");
          }
        });

      });
      </script>
   </head>
   <body class="">
       <div class="header navbar navbar-inverse ">
         <div class="navbar-inner">
            <div class="header-seperation">
               <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">
                  <li class="dropdown">
                     <a id="main-menu-toggle" href="#main-menu" class="">
                        <div class="iconset top-menu-toggle-white"></div>
                     </a>
                  </li>
               </ul>
               <a href="index.html"><img src="../assets/img/logo.png" class="logo" alt="" data-src="../assets/img/logo.png" data-src-retina="../assets/img/logo2x.png" width="106" height="21"/></a>
               <ul class="nav pull-right notifcation-center">
                  <li class="dropdown" id="header_task_bar">
                     <a href="index.html" class="dropdown-toggle active" data-toggle="">
                        <div class="iconset top-home"></div>
                     </a>
                  </li>
                  <li class="dropdown" id="header_inbox_bar">
                     <a href="email.html" class="dropdown-toggle">
                        <div class="iconset top-messages"></div>
                        <span class="badge" id="msgs-badge">2</span> 
                     </a>
                  </li>
                  <li class="dropdown" id="portrait-chat-toggler" style="display:none">
                     <a href="index.html#sidr" class="chat-menu-toggle">
                        <div class="iconset top-chat-white "></div>
                     </a>
                  </li>
               </ul>
            </div>
            <div class="header-quick-nav">
               <div class="pull-left">
                  <ul class="nav quick-section">
                     <li class="quicklinks">
                        <a href="javascript:;" class="" id="layout-condensed-toggle">
                           <div class="iconset top-menu-toggle-dark"></div>
                        </a>
                     </li>
                  </ul>
                  <ul class="nav quick-section">
                     <li class="dropdown">
                        <a href="#" class="dropdown" data-toggle="dropdown">Comprar / Vender <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="">Comprar Bitcoins</a></li>
                          <li><a href="../../vender/bitcoin">Vender Bitcoins</a></li>
                        </ul>
                      </li>

                      <li class="dropdown">
                        <a href="#" class="dropdown" data-toggle="dropdown">Depositar / Retirar <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="/buy_btc/new?locale=es">Depositar Bitcoins</a></li>
                          <li><a href="/sell_btc/new?locale=es">Retirar Bitcoins</a></li>
                          <li><a href="/buy_btc/new?locale=es">Depositar Pesos</a></li>
                          <li><a href="/sell_btc/new?locale=es">Retirar Pesos</a></li>
                        </ul>
                      </li>
                      <li class="dropdown"><a href="">Enviar</a></li>
                  </ul>
               </div>
               <div class="pull-right">

                  <ul class="nav quick-section">
                     <li><a href="/buy_btc/new?locale=es">1 BTC = $3,493 MXN</a></li>
                  </ul>

                  <ul class="nav quick-section ">
                  <li class="dropdown">
                  <a href="#" class="dropdown-toggle glyphicon glyphicon-user" style="font-size: 15px; vertical-align:middle" data-toggle="dropdown"></a>
                  <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                           <li><a href="user-profile.html"> My Account</a> </li>
                           <li><a href="calender.html">My Calendar</a> </li>
                           <li><a href="email.html"> My Inbox&nbsp;&nbsp;<span class="badge badge-important animated bounceIn">2</span></a> </li>
                           <li class="divider"></li>
                           <li><a href="login.html"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
                        </ul>
                </li>
                     
                    
                  </ul>
               </div>
            </div>
         </div>
      </div>