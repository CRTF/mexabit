<?php
session_start();

if($_SESSION['user_id']){
	
	$user_id = $_SESSION['user_id'];
	
	if($_POST['send_email'] && $_POST['send_method'] && $_POST['send_amount']){
		
		require("../connect_db.php");
		require("../functions.php");

		$email = $_POST['send_email'];
		$method = $_POST['send_method'];
		$amount = $_POST['send_amount'];

		//clean input
		$email = mysql_fix_string($email);
		$method = mysql_fix_string($method);
		$amount = mysql_fix_string($amount);

		$total_fees = 0;
		$time = date('Y-m-d G:i:s');

		$query = mysql_query("SELECT user_id , email FROM users WHERE email='$email' ");
		while($row = mysql_fetch_assoc($query)){
			$recipient_user_id = $row['user_id'];
		}

		$numRows = mysql_num_rows($query);

		if($numRows != 0){

			if($method == "mxn" || $method == "btc"){

				if(is_numeric($amount) && $amount > 0){

					$query = mysql_query("SELECT mxn_balance , btc_balance FROM balance WHERE user_id='$user_id' ");
					while($row = mysql_fetch_assoc($query)){
						$mxn_balance = $row['mxn_balance'];
						$btc_balance = $row['btc_balance'];
					}

					if($method == "mxn"){
						//check mxn balance
						if($amount <= $mxn_balance){

							$operation = "send_mxn";
							

							//get recipient mxn balance
							$query = mysql_query("SELECT mxn_balance FROM balance WHERE user_id='$recipient_user_id' ");
							while($row = mysql_fetch_assoc($query)){
								$recipient_mxn_balance = $row['mxn_balance'];								
							}

							mysql_query("SET autocommit=0");
							mysql_query("LOCK TABLES balance WRITE, send_transactions WRITE");
							//restar saldo propio
							$mxn_balance = $mxn_balance - $amount;
							mysql_query("UPDATE balance SET mxn_balance='$mxn_balance' WHERE user_id='$user_id' ");
							
							//sumar saldo a send_user
							$mxn_balance = $recipient_mxn_balance + $amount;
							mysql_query("UPDATE balance SET mxn_balance='$mxn_balance' WHERE user_id='$recipient_user_id' ");
							
							//Insert transaction
							mysql_query("INSERT INTO send_transactions VALUES ('','$user_id','$operation','$recipient_user_id','$amount','$total_fees','$time','1') ");
							mysql_query("COMMIT");
							mysql_query("UNLOCK TABLES");

							echo "<span style='color:#0AA699'>Fondos enviados con éxito</span>";

						} else {
							echo "<span style='color:#F35958'>No tienes suficientes fondos disponibles</span>";
						}

					} elseif ($method == "btc") {

						if($amount <= $btc_balance){
							
							$operation = "send_btc";
							
							//get recipient btc balance
							$query = mysql_query("SELECT btc_balance FROM balance WHERE user_id='$recipient_user_id' ");
							while($row = mysql_fetch_assoc($query)){								
								$recipient_btc_balance = $row['btc_balance'];
							}

							mysql_query("SET autocommit=0");
							mysql_query("LOCK TABLES balance WRITE, send_transactions WRITE");
							//restar saldo propio
							$btc_balance = $btc_balance - $amount;
							mysql_query("UPDATE balance SET btc_balance='$btc_balance' WHERE user_id='$user_id' ");
							
							//sumar saldo a send_user
							$btc_balance = $recipient_btc_balance + $amount;
							mysql_query("UPDATE balance SET btc_balance='$btc_balance' WHERE user_id='$recipient_user_id' ");
							
							//Insert transaction
							mysql_query("INSERT INTO send_transactions VALUES ('','$user_id','$operation','$recipient_user_id','$amount','$total_fees','$time','1') ");
							mysql_query("COMMIT");
							mysql_query("UNLOCK TABLES");

							echo "<span style='color:#0AA699'>Fondos enviados con éxito</span>";

						} else {
							echo "<span style='color:#F35958'>No tienes suficientes fondos disponibles</span>";
						}
						
					}

				} else {
					echo "<span style='color:#F35958'>Ingresa una cantidad numérica mayor a cero</span>";
				}

			} else {
				echo "<span style='color:#F35958'>El método ingresado no existe</span>";
			}

		} else {
			echo "<span style='color:#F35958'>El usuario ingresado no existe</span>";
		}

	} else {
		echo "<span style='color:#F35958'>Completa todos los campos requeridos</span>";
	}
}
?>