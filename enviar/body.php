<div class="page-container row-fluid">
   
   <!-- Sidebar -->
   <div class="page-sidebar" id="main-menu">
      <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        
         <p class="menu-title">MENÚ</p>
         <ul>
            <!-- dashboard -->
            <li class="start "> <a href="index.html"> <i class="icon-custom-home"></i> <span class="title">Dashboard</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- end dashboard -->
            <!-- Comprar/Vender -->
            <li class="">
               <a href="javascript:;"> <i class="fa fa fa-bitcoin"></i> <span class="title">Comprar / Vender</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../comprar/bitcoin">Comprar Bitcoins </a> </li>
                  <li> <a href="../vender/bitcoin">Vender Bitcoins</a> </li>
               </ul>
            </li>
            <!-- End Comprar/Vender -->
            <!-- Depositar -->
            <li class="">
               <a href="javascript:;"> <i class="fa fa fa-download"></i> <span class="title">Depositar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../depositar/bitcoin">Depositar Bitcoins </a> </li>
                  <li> <a href="../depositar/pesos">Depositar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Depositar -->
            <!-- Retirar -->
            <li >
               <a href="javascript:;"> <i class="fa fa fa-upload"></i> <span class="title">Retirar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li class="active"> <a href="../retirar/bitcoin">Retiar Bitcoins </a> </li>
                  <li> <a href="../retirar/pesos">Retirar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Retirar -->
            <!-- Enviar -->
            <li class="start active"> <a href="index.html"> <i class="fa fa-rocket"></i> <span class="title">Enviar</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- End Enviar-->
            <li class="hidden-lg hidden-md hidden-xs" id="more-widgets">
               <a href="javascript:;"> <i class="fa fa-plus"></i></a>
               <ul class="sub-menu">
                  <li class="side-bar-widgets">
                     <p class="menu-title">CUENTA <span class="pull-right"><a href="index.html#" class="create-folder"><i class="icon-plus"></i></a></span></p>
                     <ul class="folders">
                        <li>
                           <a href="index.html#">
                              <div class="status-icon green"></div>
                              Datos Personales 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon red"></div>
                              To do list 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon blue"></div>
                              Projects 
                           </a>
                        </li>
                        <li class="folder-input" style="display:none">
                           <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="" id="folder-name">
                        </li>
                     </ul>
                     <p class="menu-title">PROJECTS </p>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">Freelancer<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Redesign home page</p>
                        </div>
                     </div>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">envato<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Statistical report</p>
                        </div>
                     </div>
                  </li>
               </ul>
            </li>
         </ul>
         <div class="side-bar-widgets">
            <p class="menu-title">CUENTA</p>
            <ul class="folders">
               <li>
                  <a href="index.html#">
                     <div class="status-icon green"></div>
                     Datos Personales 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon red"></div>
                     Cuentas Bancarias 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon yellow"></div>
                     Contactos
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon blue"></div>
                     Historial
                  </a>
               </li>
               <li class="folder-input" style="display:none">
                  <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="">
               </li>
            </ul>
            <p class="menu-title">PROJECTS </p>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">Freelancer<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                  <p>Redesign home page</p>
               </div>
            </div>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">envato<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                  <p>Statistical report</p>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
   <div class="footer-widget">
      <div class="progress transparent progress-small no-radius no-margin">
         <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar"></div>
      </div>
      <div class="pull-right">
         <div class="details-status"> <span data-animation-duration="560" data-value="86" class="animate-number"></span>% </div>
         <a href="lockscreen.html"><i class="fa fa-power-off"></i></a>
      </div>
   </div>

   <!-- End Sidebar -->

   <!-- Page Content -->
   <div class="page-content">
      
      <div id="portlet-config" class="modal hide">
         <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Widget Settings</h3>
         </div>
         <div class="modal-body"> Widget settings form goes here </div>
      </div>

      <div class="clearfix"></div>


      <div class="content sm-gutter"><!-- Content sm-gutter -->
        
        <!-- Breadcrumbs -->
      	<ul class="breadcrumb">
			<li>
			<p>Enviar</p>
			</li>
			<li><a href="" class="active">Fondos</a> </li>
		</ul>
		<div class="page-title"> <i class="icon-custom-right"></i>
			<h3><span class="semi-bold">Enviar</span></h3>
		</div>
		<!-- End Breadcrumbs -->

        <div class="row">
            <div class="col-md-12">
			   <div class="grid simple">
			      <div class="grid-title no-border">
			         <h4><span class="semi-bold">Enviar fondos:</span></h4>
			         <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="javascript:;" class="remove"></a> </div>
			      	
			      	<div class="row">
						<div class="sales-graph-heading">
						<div class="col-md-5 col-sm-5">
   						<h4 class="no-margin">Tu saldo es:</h4>
   						<h5><span id="btc-balance"></span> BTC</h5>
                     <h5><span id="mxn-balance"></span> MXN</h5>
						</div>

						<div class="clearfix"></div>
						</div>
				   </div>

			      </div>
			      <div class="grid-body no-border">
			         <br>
			         <form id="sendForm" action="">
			           
			            <div class="form-group">
                        <div id="alert"></div>
			               <label class="form-label">Enviar a (e-mail del usuario) :</label>
			               
			               <div class="input-with-icon  right">
			                  <i class=""></i>
			                  <input type="email" name="send_email" id="email" class="form-control input" placeholder="usuario@email.com" required >
			               </div>
			            </div>

                     <div class="form-group">
                        <div id="alert"></div>
                        <label class="form-label">Selecciona Divisa:</label>
                        
                        <div class="input-with-icon  right">
                           <i class=""></i>
                           <select class="form-control input" name="send_method" id="method">
                              <option value="mxn">Pesos</option>
                              <option value="btc">Bitcoin</option>                              
                           </select>
                        </div>
                     </div>

                     <div class="form-group">
                        <div id="alert"></div>
                        <label class="form-label">Cantidad a enviar:</label>
                        
                        <div class="input-with-icon  right">
                           <i class=""></i>
                           <input type="text" name="send_amount" id="amount" class="form-control input" placeholder="e.j.  $5,000" required>
                        </div>
                     </div>

			           <div class="hr hr-18 dotted hr-double"></div>
			          	<div class="form-group">			          		
			          		<h5 class="form-label"><b>Fondos a enviar: <span id="order-amount"></span></b></h5><br>
                        <h5 class="form-label"><b>Divisa: <span id="order-method"></span></b></h5><br>
                        <h5 class="form-label"><b>Enviar a usuario: <span id="order-email"></span></b></h5>
			            </div>
			            <div class="form-actions">
			               <div class="pull-left">
			                  <button type="submit" class="btn btn-primary btn-cons"><i class="icon-ok"></i> Enviar Fondos</button>
			                  
			               </div>
			            </div>
			         </form>
			      </div>
			   </div>
			</div>
            
            
           
        </div>    

      </div><!-- Content sm-gutter -->
   </div><!-- End Page content -->   
</div>