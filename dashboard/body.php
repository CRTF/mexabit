<div class="page-container row-fluid">
   <!-- sidebar -->
   <div class="page-sidebar" id="main-menu">
      <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
         <!--
            <div class="user-info-wrapper">
            <div class="profile-wrapper"> <img src="assets/img/profiles/avatar.jpg" alt="" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" width="69" height="69"/> </div>
            <div class="user-info">
            <div class="greeting">Welcome</div>
            <div class="username">John <span class="semi-bold">Smith</span></div>
            <div class="status">Status<a href="index.html#">
            <div class="status-icon green"></div>
            Online</a></div>
            </div>
            </div>-->
         <p class="menu-title">MENÚ</p>
         <ul>
            <!-- dashboard -->
            <li class="active "> <a href=""> <i class="fa fa-dashboard"></i> <span class="title">Dashboard</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- end dashboard -->
            <!-- Comprar/Vender -->
            <li class="">
               <a href="javascript:;"> <i class="fa fa fa-bitcoin"></i> <span class="title">Comprar / Vender</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../comprar/bitcoin">Comprar Bitcoins </a> </li>
                  <li> <a href="../vender/bitcoin">Vender Bitcoins</a> </li>
               </ul>
            </li>
            <!-- End Comprar/Vender -->
            <!-- Depositar -->
            <li class="">
               <a href="javascript:;"> <i class="fa fa fa-download"></i> <span class="title">Depositar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../depositar/bitcoin">Depositar Bitcoins </a> </li>
                  <li> <a href="../depositar/pesos">Depositar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Depositar -->
            <!-- Retirar -->
            <li class="">
               <a href="javascript:;"> <i class="fa fa fa-upload"></i> <span class="title">Retirar</span> <span class="arrow open"></span> </a>
               <ul class="sub-menu">
                  <li> <a href="../retirar/bitcoin">Retirar Bitcoins </a> </li>
                  <li> <a href="../retirar/pesos">Retirar Pesos</a> </li>
               </ul>
            </li>
            <!-- End Retirar -->
            <!-- Enviar -->
            <li class="start "> <a href="index.html"> <i class="fa fa-rocket"></i> <span class="title">Enviar</span> <span class="selected"></span> <span class="arrow open"></span> </a></li>
            <!-- End Enviar-->
            <li class="hidden-lg hidden-md hidden-xs" id="more-widgets">
               <a href="javascript:;"> <i class="fa fa-plus"></i></a>
               <ul class="sub-menu">
                  <li class="side-bar-widgets">
                     <p class="menu-title">CUENTA <span class="pull-right"><a href="index.html#" class="create-folder"><i class="icon-plus"></i></a></span></p>
                     <ul class="folders">
                        <li>
                           <a href="index.html#">
                              <div class="status-icon green"></div>
                              Datos Personales 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon red"></div>
                              To do list 
                           </a>
                        </li>
                        <li>
                           <a href="index.html#">
                              <div class="status-icon blue"></div>
                              Projects 
                           </a>
                        </li>
                        <li class="folder-input" style="display:none">
                           <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="" id="folder-name">
                        </li>
                     </ul>
                     <p class="menu-title">PROJECTS </p>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">Freelancer<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Redesign home page</p>
                        </div>
                     </div>
                     <div class="status-widget">
                        <div class="status-widget-wrapper">
                           <div class="title">envato<a href="index.html#" class="remove-widget"><i class="icon-custom-cross"></i></a></div>
                           <p>Statistical report</p>
                        </div>
                     </div>
                  </li>
               </ul>
            </li>
         </ul>
         <div class="side-bar-widgets">
            <p class="menu-title">CUENTA</p>
            <ul class="folders">
               <li>
                  <a href="index.html#">
                     <div class="status-icon green"></div>
                     Datos Personales 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon red"></div>
                     Cuentas Bancarias 
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon yellow"></div>
                     Contactos
                  </a>
               </li>
               <li>
                  <a href="index.html#">
                     <div class="status-icon blue"></div>
                     Historial
                  </a>
               </li>
               <li class="folder-input" style="display:none">
                  <input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="">
               </li>
            </ul>
            <p class="menu-title">PRECIOS</p>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">BTC Venta:<a href="index.html#" class="remove-widget"></a></div>
                  <p><span id="sidebar-btc-sell-price"></span></p>
               </div>
            </div>
            <div class="status-widget">
               <div class="status-widget-wrapper">
                  <div class="title">BTC Compra:<a href="index.html#" class="remove-widget"></a></div>
                  <p><span id="sidebar-btc-buy-price"></span></p>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
  
   <div class="page-content">
      <div id="portlet-config" class="modal hide">
         <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Widget Settings</h3>
         </div>
         <div class="modal-body"> Widget settings form goes here </div>
      </div>
      <div class="clearfix"></div>
      <div class="content sm-gutter">
      	<ul class="breadcrumb">
		<li>
		<p>Mexabit</p>
		</li>
		<li><a href="buttons.html#" class="active">Dashboard</a> </li>
		</ul>
         <!-- conntent -->
         	<div class="page-title"> <i class="fa fa-toggle-right"></i>
				<h3>Resumen de <span class="semi-bold">Cuenta</span></h3>
			</div>

               <div id="container">
                

                  <div class="row-fluid">
                     <div class="span12">
                        <div class="grid simple ">
                           <div class="grid-title">
                              <h4><span class="semi-bold">Balances</span></h4>
                              <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="datatables.html#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                           </div>
                           <div class="grid-body ">
                              <div  class="dataTables_wrapper form-inline" role="grid">
                                 
                                 <div class="row m-b-15">
                                    <div class="col-md-6">
                                       <div class="tiles green">
                                          <div class="tiles-body">
                                             <div class="heading"> Pesos </div>
                                             <h4 style="color:#282828;"><span id="mxn-balance"> </span> MXN</h4>
                                          </div>
                                          <div class="tile-footer">
                                             <div class="iconplaceholder" style="padding-right:20px"><i class="fa fa-repeat" ></i></div>
                                             Actualizar
                                          </div>
                                       </div>   
                                    </div>

                                    <div class="col-md-6">
                                       <div class="tiles red">
                                          <div class="tiles-body">
                                             <div class="heading"> Bitcoins </div>
                                             <h4 style="color:#282828;"><span id="btc-balance"> </span> BTC</h4>
                                          </div>
                                          <div class="tile-footer">
                                             <div class="iconplaceholder" style="padding-right:20px"><i class="fa fa-repeat"></i></div>
                                             Actualizar
                                          </div>
                                       </div>   
                                    </div>
                                 </div>


                                 <div class="row">
                                    <div class="col-md-6">
                                      <a  href="../comprar/bitcoin"> <button type="button " class="btn btn-primary btn-cons" style="width:100%"><h3 class="text-center text-white m-b-15"><span class="semi-bold">Comprar Bitcoins</span></h3></button></a>
                                    </div>
                                    <div class="col-md-6">
                                       <button type="button " class="btn btn-danger btn-cons" style="width:100%"><h3 class="text-center text-white m-b-15"><span class="semi-bold">Vender Bitcoins</span></h3></button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


                  <div class="row-fluid">
                     <div class="span12">
                        <div class="grid simple ">
                           <div class="grid-title">
                              <h4>Operaciones <span class="semi-bold">recientes</span></h4>
                              <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="datatables.html#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                           </div>
                           <div class="grid-body ">
                              <div id="example3_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                 
                                 <table class="table table-hover no-more-tables">
                                    <thead>
                                       <tr role="row">
                                          <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 144px;" aria-label="Rendering engine">Rendering engine</th>
                                          <th class="sorting" role="columnheader" tabindex="0" aria-controls="example3" rowspan="1" colspan="1" style="width: 192px;" aria-label="Browser: activate to sort column ascending">Browser</th>
                                          <th class="sorting" role="columnheader" tabindex="0" aria-controls="example3" rowspan="1" colspan="1" style="width: 177px;" aria-label="Platform(s): activate to sort column ascending">Platform(s)</th>
                                          <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="example3" rowspan="1" colspan="1" style="width: 124px;" aria-sort="descending" aria-label="Engine version: activate to sort column ascending">Engine version</th>
                                          <th class="sorting" role="columnheader" tabindex="0" aria-controls="example3" rowspan="1" colspan="1" style="width: 87px;" aria-label="CSS grade: activate to sort column ascending">CSS grade</th>
                                       </tr>
                                    </thead>
                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                       <tr>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                       </tr>
                                        <tr>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


         <!-- end content -->    


      </div>
   </div>

</div>