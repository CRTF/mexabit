<?php
session_start();
if($_SESSION['user_id']){
	require_once("header.php");
	require_once("body.php");
	require_once("footer.php");
} else {
	header("Location: ../login");
	exit();
}
?>