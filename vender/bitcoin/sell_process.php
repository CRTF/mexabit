<?php
session_start();
if($_SESSION['user_id']){
	$user_id = $_SESSION['user_id'];

	if($_POST['sell_amount']){

		$amount = $_POST['sell_amount'];

		if(is_numeric($amount) && $amount > 0){

			require("../../connect_db.php");
			require("../../functions.php");
			$amount = mysql_fix_string($amount);

			//get user balance, price and comision

			#get buy price
			$query = mysql_query("SELECT btc_buy_price , fees FROM price");
			while($row = mysql_fetch_assoc($query)){
				$btc_buy_price = $row['btc_buy_price'];
				$fees = $row['fees'];
			}

			$query = mysql_query("SELECT mxn_balance , btc_balance FROM balance WHERE user_id='$user_id' ");
			while($row = mysql_fetch_assoc($query)){
				$mxn_balance = $row['mxn_balance'];
				$btc_balance = $row['btc_balance'];
			}
			
			

			if($amount <= $btc_balance){
				$iva = 1.16;
				$time = date('Y-m-d G:i:s');
				$operation = 'btc_sell';

				$subtotal = $btc_buy_price * $amount;
				//Calcular comision
				$total_fees = $subtotal * $fees * $iva;
				//Calcular cantidad neta
				$total_amount = $subtotal - $total_fees;
				
				$new_mxn_balance = $mxn_balance + $total_amount;
				$new_btc_balance = $btc_balance - $amount;

				//Cambiar balances
				mysql_query("SET autocommit=0");
				mysql_query("LOCK TABLES balance WRITE, btc_transactions WRITE");
				mysql_query("UPDATE balance SET mxn_balance='$new_mxn_balance' , btc_balance='$new_btc_balance' WHERE user_id='$user_id' ");
				mysql_query("INSERT INTO btc_transactions VALUES ('','$user_id','$operation','$btc_buy_price','$amount','$total_fees','$time','0') ");
				mysql_query("COMMIT");
				mysql_query("UNLOCK TABLES");
				

				echo "<span style='color:#0AA699'> La operación se ha realizado con éxito </span>";

			} else {
				echo "<span style='color:#F35958'>No tienes suficientes fondos disponibles</span>";
			}

		} else {
			echo "<span style='color:#F35958'>Introduce un valor numérico mayor a cero</span>";
		}

	} else {
		echo "<span style='color:#F35958'>Completa todos los campos requeridos</span>";
	}

}
?>