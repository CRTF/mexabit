-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2015 at 11:38 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mexabit`
--

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE IF NOT EXISTS `balance` (
  `user_id` int(11) NOT NULL,
  `mxn_balance` decimal(16,8) NOT NULL,
  `btc_balance` decimal(16,8) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balance`
--

INSERT INTO `balance` (`user_id`, `mxn_balance`, `btc_balance`) VALUES
(1, 4900.00000000, 1.00000000);

-- --------------------------------------------------------

--
-- Table structure for table `btc_deposits`
--

CREATE TABLE IF NOT EXISTS `btc_deposits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(144) NOT NULL,
  `operation` varchar(144) NOT NULL,
  `amount` decimal(16,8) NOT NULL,
  `txid` varchar(144) NOT NULL,
  `time` int(11) NOT NULL,
  `confirmations` int(11) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btc_transactions`
--

CREATE TABLE IF NOT EXISTS `btc_transactions` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `operation` varchar(144) NOT NULL,
  `price` decimal(16,8) NOT NULL,
  `amount` decimal(16,8) NOT NULL,
  `total_fees` decimal(16,8) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `btc_transactions`
--

INSERT INTO `btc_transactions` (`order_id`, `user_id`, `operation`, `price`, `amount`, `total_fees`, `time`, `admin`) VALUES
(1, 1, 'btc_buy', 3457.00000000, 100.00000000, 1.16000000, '2015-02-01 23:41:26', 0),
(2, 1, 'btc_buy', 3457.00000000, 100.00000000, 1.16000000, '2015-02-01 23:43:12', 0),
(3, 1, 'btc_buy', 3457.00000000, 100.00000000, 1.16000000, '2015-02-01 23:46:34', 0),
(4, 1, 'btc_buy', 3457.00000000, 1.00000000, 0.01160000, '2015-02-01 23:47:13', 0),
(5, 1, 'btc_buy', 3457.00000000, 1.00000000, 0.01160000, '2015-02-01 23:47:16', 0),
(6, 1, 'btc_sell', 3320.00000000, 0.01000000, 0.38512000, '2015-02-02 00:35:39', 0),
(7, 1, 'btc_sell', 3320.00000000, 0.10000000, 3.85120000, '2015-02-02 00:37:09', 0),
(8, 1, 'btc_buy', 3457.00000000, 100.00000000, 1.16000000, '2015-02-02 00:38:40', 0);

-- --------------------------------------------------------

--
-- Table structure for table `btc_withdraws`
--

CREATE TABLE IF NOT EXISTS `btc_withdraws` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `operation` varchar(144) NOT NULL,
  `amount` decimal(16,8) NOT NULL,
  `to_address` varchar(144) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hash` varchar(144) NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `btc_withdraws`
--

INSERT INTO `btc_withdraws` (`order_id`, `user_id`, `operation`, `amount`, `to_address`, `time`, `hash`, `confirmed`, `completed`) VALUES
(9, 1, 'btc_withdraw', 0.09990000, '18zNChczsXRYCdYANcRhM2zLPMQsDTL6zh', '2015-02-02 13:10:21', '7f44b9827e931133df08cf9a1f364bad', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mxn_withdraws`
--

CREATE TABLE IF NOT EXISTS `mxn_withdraws` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `operation` varchar(144) NOT NULL,
  `amount` decimal(16,8) NOT NULL,
  `clabe` varchar(144) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titular` varchar(144) NOT NULL,
  `bank` varchar(144) NOT NULL,
  `total_fees` decimal(16,8) NOT NULL,
  `hash` varchar(144) NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mxn_withdraws`
--

INSERT INTO `mxn_withdraws` (`order_id`, `user_id`, `operation`, `amount`, `clabe`, `time`, `titular`, `bank`, `total_fees`, `hash`, `confirmed`, `completed`) VALUES
(1, 1, 'mxn_withdraw', 99.00000000, '1', '2015-02-02 13:25:46', '13', '123', 1.00000000, 'f29c64f701359fabdf138420b70a372c', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fees` decimal(16,8) NOT NULL,
  `btc_sell_price` decimal(16,8) NOT NULL,
  `btc_buy_price` decimal(16,8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id`, `fees`, `btc_sell_price`, `btc_buy_price`) VALUES
(1, 0.01000000, 3383.20500000, 3380.52480000);

-- --------------------------------------------------------

--
-- Table structure for table `price_history`
--

CREATE TABLE IF NOT EXISTS `price_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mexabit_btc_sell` decimal(16,8) NOT NULL,
  `mexabit_btc_buy` decimal(16,8) NOT NULL,
  `volabit_btc_sell` decimal(16,8) NOT NULL,
  `volabit_btc_buy` decimal(16,8) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `price_history`
--

INSERT INTO `price_history` (`id`, `mexabit_btc_sell`, `mexabit_btc_buy`, `volabit_btc_sell`, `volabit_btc_buy`, `time`) VALUES
(1, 3383.20500000, 3380.52480000, 3452.25000000, 3314.24000000, '2015-02-02 21:36:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(144) NOT NULL,
  `password` varchar(144) NOT NULL,
  `btc_address` varchar(144) NOT NULL,
  `drk_address` varchar(144) NOT NULL,
  `hash` varchar(144) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `btc_address`, `drk_address`, `hash`, `activated`) VALUES
(1, 'crt.ferguson@gmail.com', '123', '147A1jBbqSiKD7v5g2mu2pGmbj27TJGTsR', '', '', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
