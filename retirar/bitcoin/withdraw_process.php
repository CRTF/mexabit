<?php
session_start();
if($_SESSION['user_id']){
$user_id = $_SESSION['user_id'];
$address = $_POST['withdraw_address'];
$amount = $_POST['withdraw_amount'];

	if($address && $amount){		

		if(is_numeric($amount) && $amount > 0){
			
			require("../../daemon/bitcoin/functions.php");
			if(validateAddress($address)){
				//check if bitcoin address is valid
				require("../../connect_db.php");
				require("../../functions.php");

				$address = mysql_fix_string($address);
				$amount = mysql_fix_string($amount);
				$fees = 0.0001;			
				$time = date('Y-m-d G:i:s');
				$operation = 'btc_withdraw';
				$hash = md5(uniqid(mt_rand(), true)); 

				$query = mysql_query("SELECT btc_balance FROM balance WHERE user_id='$user_id' ");
				while($row = mysql_fetch_assoc($query)){
					$btc_balance = $row['btc_balance'];
				}
					
				$total = $amount - $fees;

				if( $amount < $btc_balance){

					$query = mysql_query("SELECT * FROM btc_withdraws WHERE user_id='$user_id' AND confirmed='0' AND time >= now() - INTERVAL 15 MINUTE");
					$numRows = mysql_num_rows($query);
					if($numRows == 0){
						#get email
						$query = mysql_query("SELECT email FROM users WHERE user_id='$user_id' ");
						while($row = mysql_fetch_assoc($query)){
							$email = $row['email'];
						}

						//$new_btc_balance = $btc_balance - $total;
						mysql_query("SET autocommit=0");
						mysql_query("LOCK TABLES btc_withdraws WRITE");
						//mysql_query("UPDATE balance SET btc_balance='$new_btc_balance' WHERE user_id='$user_id'  ");
						mysql_query("INSERT INTO btc_withdraws VALUES ('','$user_id','$operation','$total','$address','$time','$hash','0','0') ");
						mysql_query("COMMIT");
						mysql_query("UNLOCK TABLES");

						#send mail
						$subject = 'Correo de confirmación';
						$message = "
						Hola,\n\n
						Confirma tu retiro con el link de abajo:\n				
						https://www.mexabit.com/retirar/bitcoin/confirmacion?id=$user_id&hash=$hash
						\n
						Gracias!
						";
						mail($email, $subject, $message);
						sleep(1);
						
						echo "<span style='color:#0AA699'>En breve recibirás un correo de confirmación</span>";
					
					} else {
						echo $alert = "<span style='color:#F35958'>Por seguridad solo puedes hacer una petición de retiro cada 15 minutos hasta que confirmes las ateriores.</span>";
					}

				} else {
					echo "<span style='color:#F35958'>No tienes suficientes fondos disponibles $btc_balance</span>";
				}	
			} else {
				echo "<span style='color:#F35958'>Ingresa una dirección Bitcoin válida</span>";
			}	

		} else {
			echo "<span style='color:#F35958'>Ingresa un valor numérico mayor a cero</span>";
		}

	} else {
		echo "<span style='color:#F35958'>Completa todos los campos requeridos</span>";
	}
}
?>
